package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.beans.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();


    private int generateId() {
        return employees.stream().max(Comparator.comparingLong(Employee::getId)).map((employee -> employee.getId() + 1)).orElse(1);
    }

    public Employee save(Employee employee) {
        employee.setId(generateId());
        this.employees.add(employee);
        return employee;
    }

    public List<Employee> getAllEmployees() {
        return this.employees;
    }

    public void clearEmployees() {
        this.employees.clear();
    }

    public Employee getEmployeeById(Integer id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst().orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployeeByCompanyId(Integer companyId, Employee employee) {
        for (Employee emp : employees) {
            if (emp.getCompanyId() == companyId) {
                emp.setAge(employee.getAge());
                emp.setSalary(employee.getSalary());
                emp.setGender(emp.getGender());
                // stream api 无法提前终止循环，使用可以提前终止的api也无法引用外部的非有效常量。
                return emp;
            }
        }
        return null;
    }

    public Employee deleteEmployeeById(Integer id) {
        for (int i = 0; i < employees.size(); i++) {
            if (employees.get(i).getId() == id) {
                return employees.remove(i);
            }
        }
        return null;
    }

    public List<Employee> getEmployeesWithPagination(Integer page, Integer size) {
        return employees.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    public Employee updateEmployeeById(Integer id, Employee employee) {
        Employee updatedEmployee = null;
        for (Employee emp : employees) {
            if (emp.getId() == id) {
                emp.setAge(employee.getAge());
                emp.setSalary(employee.getSalary());
                updatedEmployee = emp;
                // stream api 无法提前终止循环，使用可以提前终止的api也无法引用外部的非有效常量。
                break;
            }
        }
        return updatedEmployee;
    }
}
