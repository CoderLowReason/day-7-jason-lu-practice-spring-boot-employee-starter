package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.beans.Company;
import com.thoughtworks.springbootemployee.beans.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private List<Company>  companies = new ArrayList<>();

    private int generateId() {
        return companies.stream().max(Comparator.comparingLong(Company::getId)).map((employee -> employee.getId() + 1)).orElse(1);
    }
    public List<Company> getCompanies() {
        return this.companies;
    }

    public Company save(Company company) {
        company.setId(generateId());
        this.companies.add(company);
        return company;
    }

    public Company getCompanyById(Integer id) {
        return companies.stream().filter(company -> company.getId() == id).findAny().orElse(null);
    }

    public Company deleteCompanyById(Integer id) {
        for (int i = 0; i < companies.size(); i++) {
            if (companies.get(i).getId() == id) return companies.remove(i);
        }
        return null;
    }

    public Company updateCompanyById(Integer id, Company company) {
        for (Company updatedCompany : companies) {
            if (updatedCompany.getId() == id) {
                updatedCompany.setName(company.getName());
                return updatedCompany;
            }
        }
        return null;
    }

    public List<Company> getCompaniesWithPagination(Integer page, Integer size) {
        return companies.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
