package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.beans.Company;
import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @PostMapping
    public Company addCompany(@RequestBody Company company) {
        if (company == null) return null;
        return companyService.save(company);
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyService.getCompanies();
    }

//    @PutMapping("{companyId}")
//    public Employee updateEmployeeByCompanyId(@PathVariable("companyId") Integer companyId, @RequestBody Employee employee) {
//        if (companyId == null || employee == null) return null;
//        return repository.updateEmployeeByCompanyId(companyId, employee);
//    }

    @GetMapping("{id}")
    public Company getCompanyById(@PathVariable("id") Integer id) {
        return companyService.getCompanyById(id);
    }

    @DeleteMapping("{id}")
    public Company deleteCompanyById(@PathVariable("id") Integer id) {
        return companyService.deleteCompanyById(id);
    }

    @PutMapping("{id}")
    public Company updateCompanyById(@PathVariable("id") Integer id, @RequestBody Company company) {
        if (id == null || company == null) return null;
        return companyService.updateCompanyById(id, company);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesWithPagination(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        if (page == null || size == null) return null;
        return companyService.getCompaniesWithPagination(page, size);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable("companyId") Integer companyId) {
        if (companyId == null) return null;
        return companyService.getEmployeesByCompanyId(companyId);
    }
}
