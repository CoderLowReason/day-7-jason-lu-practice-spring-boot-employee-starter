package com.thoughtworks.springbootemployee.exception;

public class EmployeeCreatedException extends RuntimeException{
    public EmployeeCreatedException(String errMsg) {
        super(errMsg);
    }
}
