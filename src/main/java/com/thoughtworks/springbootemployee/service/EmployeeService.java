package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    public static final String EMPLOYEE_AGE_RANGE_ERROR_MESSAGE = "Employee must be 18~65 years old";
    public static final String EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE = "Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created";
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) {
        if (employee == null) return null;
        int age = employee.getAge();
        if (isAgeRangeValid(age)) throw new EmployeeCreatedException(EMPLOYEE_AGE_RANGE_ERROR_MESSAGE);
        if (isAgeSalaryValid(employee, age)) throw new EmployeeCreatedException(EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE);
        employee.setActive(true);
        return employeeRepository.save(employee);
    }

    private static boolean isAgeSalaryValid(Employee employee, int age) {
        return age >= 30 && employee.getSalary() < 20000;
    }

    private static boolean isAgeRangeValid(int age) {
        return age > 65 || age < 18;
    }

    public void deleteEmployeeById(Integer id) {
        Employee employee = employeeRepository.getEmployeeById(id);
        employee.setActive(false);
    }

    public Employee updateEmployeeById(Integer id, Employee employee) {
        Employee updateTobe = employeeRepository.getEmployeeById(id);
        if (employee.isActive()) {
            updateTobe.setAge(employee.getAge());
            updateTobe.setName(employee.getName());
            updateTobe.setSalary(employee.getSalary());
            updateTobe.setCompanyId(employee.getCompanyId());
            updateTobe.setGender(employee.getGender());
        }
        System.out.println(employeeRepository.getEmployeeById(id));
        return updateTobe;
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.getAllEmployees();
    }

    public Employee getEmployeeById(Integer id) {
        return employeeRepository.getEmployeeById(id);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeeRepository.getEmployeeByGender(gender);
    }

    public List<Employee> getEmployeesWithPagination(Integer page, Integer size) {
        return employeeRepository.getEmployeesWithPagination(page, size);
    }
}
