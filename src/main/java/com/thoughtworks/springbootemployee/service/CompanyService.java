package com.thoughtworks.springbootemployee.service;


import com.thoughtworks.springbootemployee.beans.Company;
import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }


    public Company save(Company company) {
        return companyRepository.save(company);
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(Integer id) {
        return companyRepository.getCompanyById(id);
    }

    public Company deleteCompanyById(Integer id) {
        return companyRepository.deleteCompanyById(id);
    }

    public Company updateCompanyById(Integer id, Company company) {
        return companyRepository.updateCompanyById(id, company);
    }

    public List<Company> getCompaniesWithPagination(Integer page, Integer size) {
        return companyRepository.getCompaniesWithPagination(page, size);
    }

    public List<Employee> getEmployeesByCompanyId(Integer companyId) {
        List<Employee> employees = employeeRepository.getAllEmployees();
        return employees.stream().filter(employee -> employee.getCompanyId() == companyId).collect(Collectors.toList());
    }
}
