package com.thoughtworks.springbootemployee.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private int id;
    private int age;
    private String name;
    private String gender;
    private int salary;
    private int companyId;
    private boolean isActive;
}
