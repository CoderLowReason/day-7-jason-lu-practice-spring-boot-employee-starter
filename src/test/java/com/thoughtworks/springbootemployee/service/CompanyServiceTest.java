package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CompanyServiceTest {

    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private final CompanyRepository companyRepository = mock(CompanyRepository.class);

    private final CompanyService companyService = new CompanyService(companyRepository, employeeRepository);


    @Test
    void should_return_employees_in_specific_company_when_getEmployeesByCompanyId_given_companyId() {
        //given
        Employee employee1InCompany1 = new Employee(1, 19, "Lily", "female", 20000, 1, true);
        Employee employee2InCompany1 = new Employee(1, 19, "Sarah", "female", 20001, 1, true);
        Employee employee3InCompany1 = new Employee(1, 19, "Jason", "male", 20002, 1, true);
        Employee employee4InCompany2 = new Employee(1, 19, "Adam", "male", 20004, 2, true);
        Employee employee5InCompany2 = new Employee(1, 19, "Mary", "female", 20005, 2, true);
        List<Employee> employeeList = new ArrayList<>(Arrays.asList(employee1InCompany1, employee2InCompany1, employee3InCompany1, employee4InCompany2, employee5InCompany2));
        //when
        when(employeeRepository.getAllEmployees()).thenReturn(employeeList);
        //then
        List<Employee> employeesResponse = companyService.getEmployeesByCompanyId(2);

        assertEquals(2, employeesResponse.size());
        assertEquals("Adam", employeesResponse.get(0).getName());
        assertEquals("Mary", employeesResponse.get(1).getName());
    }

}
