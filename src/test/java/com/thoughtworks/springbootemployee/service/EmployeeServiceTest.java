package com.thoughtworks.springbootemployee.service;


import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.exception.EmployeeCreatedException;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EmployeeServiceTest {

    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private final EmployeeService employeeService = new EmployeeService(employeeRepository);

    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_created_employee_when_create_employee_given_employee_age_is_between_18_to_56(Integer age) {
        //given
        Employee employee = new Employee(1, age, "Lily", "female", 20000, 1, true);
        //when
        when(employeeRepository.save(eq(employee))).thenReturn(new Employee(1, age,"Lily","female",20000, 1, true));
        //then
        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1, employeeResponse.getCompanyId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(age, employeeResponse.getAge());
    }

    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_to_65(Integer age) {
        //given
        Employee employee = new Employee(1, age, "Lily", "female", 20000, 1, true);
        //when
        //then
        assertThrows(EmployeeCreatedException.class, ()-> employeeService.create(employee));
    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30,20000", "29,19999"})
    void should_return_create_employee_when_create_employee_given_(Integer age, Integer salary) {
        //given
        Employee employee = new Employee(1, age, "Lily", "female", salary, 1, true);
        //when
        when(employeeRepository.save(employee)).thenReturn(new Employee(1, age, "Lily", "female", salary, 1, true));
        //then
        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1, employeeResponse.getCompanyId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(age, employeeResponse.getAge());
        assertEquals(salary, employeeResponse.getSalary());
    }

    @Test
    void should_throw_exception_when_create_employee_given_age_is_30_salary_is_19999() {
        //given
        Employee employee = new Employee(1, 30, "Lily", "female", 19999, 1, true);
        //when
        when(employeeRepository.save(employee)).thenReturn(new Employee(1, 30, "Lily", "female", 19999, 1, true));
        //then
        EmployeeCreatedException employeeCreatedException = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));
        assertEquals("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created", employeeCreatedException.getMessage());
    }

    @Test
    void should_set_active_status_when_perform_create_employee_given_employee() {
        Employee employee = new Employee(1, 30, "Lily", "female", 20000, 1, false);
        when(employeeRepository.save(employee)).thenReturn(new Employee(1, 30, "Lily", "female", 20000, 1, true));
        employeeService.create(employee);
        assertTrue(employee.isActive());
    }

    @Test
    void should_set_not_active_when_delete_an_employee_given_an_employee() {
        Employee employee = new Employee(1, 30, "Lily", "female", 20000, 1, true);
        when(employeeRepository.getEmployeeById(1)).thenReturn(employee);
        employeeService.deleteEmployeeById(1);
        assertFalse(employee.isActive());
    }

    @Test
    void should_update_an_employee_when_update_given_an_active_employee() {
        Employee employee = new Employee(1, 30, "Lily", "female", 20000, 1, true);
        Employee updatedEmployee = new Employee(1, 333, "Shelly", "female", 30000, 1, true);

        when(employeeRepository.getEmployeeById(1)).thenReturn(employee);
        Employee responseEmployee = employeeService.updateEmployeeById(1, updatedEmployee);

        assertEquals(updatedEmployee, responseEmployee);
    }

    @Test
    void should_not_update_an_employee_when_update_given_an_disActive_employee() {
        Employee employee = new Employee(1, 30, "Lily", "female", 20000, 1, false);
        Employee updatedEmployee = new Employee(1, 333, "Shelly", "female", 30000, 1, false);

        when(employeeRepository.getEmployeeById(1)).thenReturn(employee);
        Employee responseEmployee = employeeService.updateEmployeeById(1, updatedEmployee);

        assertEquals(responseEmployee, employee);
    }

}
