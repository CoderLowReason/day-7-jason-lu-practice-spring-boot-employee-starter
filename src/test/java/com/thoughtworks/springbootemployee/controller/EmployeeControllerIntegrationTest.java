package com.thoughtworks.springbootemployee.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerIntegrationTest {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    private MockMvc client;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void afterEach() {
        employeeRepository.clearEmployees();
    }

    @Test
    void should_get_the_saved_employee_when_save_given_an_employee() throws Exception {
        //given
        Employee employee = new Employee();
        employee.setAge(18);
        employee.setName("Jason");
        employee.setSalary(11000);
        employee.setGender("male");
        employee.setCompanyId(1);
        String employeeJsonStr = new ObjectMapper().writeValueAsString(employee);
        //when
        client.perform(MockMvcRequestBuilders.post("/employees").contentType(MediaType.APPLICATION_JSON).content(employeeJsonStr))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Jason"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(11000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(1));
        //then
        List<Employee> allEmployees = employeeRepository.getAllEmployees();
        assertEquals(18, allEmployees.get(0).getAge());
        assertEquals("Jason", allEmployees.get(0).getName());
        assertEquals(11000, allEmployees.get(0).getSalary());
        assertEquals(1, allEmployees.get(0).getCompanyId());
        assertEquals("male", allEmployees.get(0).getGender());
    }

    @Test
    void should_get_an_employee_when_getEmployeeById_given_an_employeeId() throws Exception {
        //given
        Employee employee = new Employee();
        employee.setAge(18);
        employee.setName("Jason");
        employee.setSalary(11000);
        employee.setGender("male");
        employee.setCompanyId(1);
        employeeRepository.save(employee);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Jason"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(11000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(18));
    }

    @Test
    void should_update_salary_name_in_employee_when_updateEmployeeById_given_an_employeeId_and_an_employee() throws Exception {
        //given
        Employee employee = new Employee();
        employee.setAge(18);
        employee.setName("Jason");
        employee.setSalary(11000);
        employee.setGender("male");
        employee.setCompanyId(1);
        employee.setActive(true);
        employeeRepository.save(employee);

        Employee update = new Employee();
        update.setAge(30);
        update.setName("Jason");
        update.setSalary(22000);
        update.setGender("male");
        update.setCompanyId(1);
        update.setActive(true);
        String updateJsonStr = new ObjectMapper().writeValueAsString(update);
        //when

        //then
        client.perform(MockMvcRequestBuilders.put("/employees/1").contentType(MediaType.APPLICATION_JSON).content(updateJsonStr))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Jason"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(22000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(30));

        Employee actualEmployee = employeeRepository.getEmployeeById(1);
        assertEquals(30, actualEmployee.getAge());
        assertEquals(22000, actualEmployee.getSalary());
    }

    @Test
    void should_get_all_employees_when_getAllEmployees() throws Exception {
        //given
        Employee employee1 = new Employee(1, 18, "Jason", "male", 11000, 1, true);
        Employee employee2 = new Employee(2, 28, "Jerry", "male", 22000, 1, true);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Jason"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(11000))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Jerry"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").value(22000))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].companyId").value(1));;
    }

    @Test
    void should_get_all_female_employees_when_getEmployeeByGender_given_gender() throws Exception {
        //given
        Employee maleEmployee1 = new Employee(1, 18, "Jason", "male", 11000, 1, true);
        Employee maleEmployee2 = new Employee(2, 28, "Jerry", "male", 22000, 1, true);
        Employee femaleEmployee1 = new Employee(3, 18, "Ellie", "female", 33000, 1, true);
        employeeRepository.save(maleEmployee1);
        employeeRepository.save(maleEmployee2);
        employeeRepository.save(femaleEmployee1);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Ellie"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(33000))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(1));
    }

    @Test
    void should_delete_an_employee_when_deleteEmployeeById() throws Exception {
        //given
        Employee employee1 = new Employee(1, 18, "Jason", "male", 11000, 1, true);
        Employee employee2 = new Employee(2, 28, "Jerry", "male", 22000, 1, true);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        //when
        client.perform(MockMvcRequestBuilders.delete("/employees/1"));
        //then
        Employee employee = employeeRepository.getEmployeeById(1);
        assertFalse(employee.isActive());
    }

    @Test
    void should_get_employees_by_page_when_getEmployeesWithPagination() throws Exception {
        //given
        Employee employee1 = new Employee(1, 18, "Jason", "male", 11000, 1, true);
        Employee employee2 = new Employee(2, 28, "Jerry", "male", 22000, 1, true);
        Employee employee3 = new Employee(3, 38, "Emily", "female", 33000, 1, true);
        Employee employee4 = new Employee(4, 48, "Silly", "male", 44000, 1,true);
        Employee employee5 = new Employee(5, 58, "Jacky", "female", 55000, 1, true);

        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        employeeRepository.save(employee4);
        employeeRepository.save(employee5);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees?page=2&size=2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Emily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Silly"));
    }
}
