package com.thoughtworks.springbootemployee.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.beans.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    private MockMvc client;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void should_call_service_when_perform_post_employee_given_employee() throws Exception {
        Employee employee = new Employee(1, 30, "Lily", "female", 19999, 1, true);
        String employeeJsonStr = objectMapper.writeValueAsString(employee);
        client.perform(post("/employees").contentType(MediaType.APPLICATION_JSON).content(employeeJsonStr))
                .andExpect(status().isCreated());
        // 证明employService调用了create方法
        verify(employeeService).create(employee);
    }
}
