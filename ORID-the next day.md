### 1.Objectives

1. Code review.The difference between orElseGet and orElse function is that when you just simply return an expected value object, use orElse, but when you wanna call a function to return an expected object,then you use orElseGet.
2. I review the concept of IOC(Inversion of Controll) and DI(Dependency Inject).I've learned it before, so it was quite easy for me to catch up with the teacher.
3. I've learned the Integration Test.The test in controller, the test in service, the test in repository.
4. I've reviewed the annotations about unit test today and learned some new annotations, especially "@ParameterizedTest"、"@ValueSource"、"@CsvSource" and so on.
5. I've done a lot unit test in the past intern period.So i was able to catch up with the teacher and help my teammate.

### 2.Reflective

​	 gainful and satisfied.

### 3. Interpretive

​	the most impressive thing is that i have learned a lot of new annotations about unit test today, such as "@ParameterizedTest"、"@ValueSource"、"@CsvSource"， which makes me so happy. I wish to learn more.

### 4. Decisional

    learn more about the unit test. the more the better.
